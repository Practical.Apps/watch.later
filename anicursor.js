(function() {
    // Create and inject CSS
  const frames = [
    "/cursor/xmastreeframe1.png",
    "/cursor/xmastreeframe2.png",
    "/cursor/xmastreeframe3.png",
    "/cursor/xmastreeframe4.png",
    "/cursor/xmastreeframe5.png"
  ];

  const cursor = document.getElementById("custom-cursor");
  let currentFrame = 0;

  // Preload images
  const images = [];
  for (let i = 0; i < frames.length; i++) {
    images[i] = new Image();
    images[i].src = frames[i];
  }

  // Animate cursor
  setInterval(() => {
    cursor.style.backgroundImage = `url(${frames[currentFrame]})`;
    currentFrame = (currentFrame + 1) % frames.length;
  }, 300); // Adjust interval as needed

  // Follow mouse movement
  document.addEventListener("mousemove", (e) => {
    cursor.style.left = e.pageX + "px";
    cursor.style.top = e.pageY + "px";
  });
})();
